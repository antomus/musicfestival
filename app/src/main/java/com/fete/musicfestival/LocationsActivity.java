package com.fete.musicfestival;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import com.fete.musicfestival.models.Dot;

import com.fete.musicfestival.utils.LocationGridAdapter;

import java.util.List;


public class LocationsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        super.setCustomActionBar();
        super.registerReceiver();
        showData(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void showData(String search) {
        List<Dot> dots = Dot.listAll(Dot.class);
        GridView gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new LocationGridAdapter(this, dots));
    }
}
