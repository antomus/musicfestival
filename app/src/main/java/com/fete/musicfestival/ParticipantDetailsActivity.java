package com.fete.musicfestival;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fete.musicfestival.models.Dot;
import com.fete.musicfestival.models.Participant;
import com.squareup.picasso.Picasso;


public class ParticipantDetailsActivity extends BaseActivity {
    protected Long id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.participant_details);
        super.setCustomActionBar();
        this.id = getIntent().getLongExtra("id", 0);

        Participant participant = Participant.findById(Participant.class, id);
        if (participant != null) {
            TextView tv = (TextView) findViewById(R.id.description);
            TextView td = (TextView) findViewById(R.id.date);
            TextView nm = (TextView) findViewById(R.id.name);
            ImageView imgView = (ImageView) findViewById(R.id.image);
            Picasso.with(this).load(participant.getImg()).into(imgView);
            nm.setText(Html.fromHtml(participant.getName()));
            td.setText(participant.getStart() + "  -  " + participant.getFinish());
            tv.setText(Html.fromHtml(participant.getDesc()));  // using html
            tv.invalidate(); // call this to render the text
            nm.invalidate();
            td.invalidate();

        }


    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
        if(this.id > 0) {
            getMenuInflater().inflate(R.menu.menu_participant_details, menu);
        }
        return super.onCreateOptionsMenu(menu);
	}


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_map) {
            Dot dot = Participant.findById(Participant.class, this.id).getDot();
            Intent intent = new Intent(getBaseContext(), MapActivity.class);
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra("dotId", dot.getId());
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void showData(String search){

    }

}
