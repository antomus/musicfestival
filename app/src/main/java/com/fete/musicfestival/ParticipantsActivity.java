package com.fete.musicfestival;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.fete.musicfestival.models.Dot;
import com.fete.musicfestival.models.Participant;
import com.fete.musicfestival.utils.LocationGridAdapter;
import com.fete.musicfestival.utils.MyGridAdapter;

import java.util.List;
import static com.fete.musicfestival.utils.Constants.PRINT_DEBUG;


public class ParticipantsActivity extends BaseActivity {

    protected Long dotId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.participants_layout);
        super.setCustomActionBar();
        super.registerReceiver();

        showData(null);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(this.dotId > 0) {
           return true;
        }
        getMenuInflater().inflate(R.menu.menu_participants, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_map) {
            Intent intent = new Intent(getBaseContext(), MapActivity.class);
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra("dotId", this.dotId);
            startActivity(intent);
        }if (id == R.id.search) {
            super.showEditBox();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void showData(String search) {
        List<Participant> participantsList;

        if(search != null && !search.isEmpty()) {
            participantsList = Participant.getParticipantsFilteredByName(search);
        } else {
            this.dotId = getIntent().getLongExtra("dotId", 0);
            //Log.e("Participants: dotId", dotId + "");

            if (dotId > 0) {
                participantsList = Participant.getParticipantsByDotId(dotId);
            } else {
                participantsList = Participant.listAll(Participant.class);
            }
        }

        if(PRINT_DEBUG)Log.e("participantsList.size()", participantsList.size() + "");
        GridView gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(new MyGridAdapter(this, participantsList));
    }
}
