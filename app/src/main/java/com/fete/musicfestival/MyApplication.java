package com.fete.musicfestival;

import com.fete.musicfestival.utils.MyLifecycleHandler;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by antomus on 4/4/15.
 */
public class MyApplication extends com.orm.SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
       // registerActivityLifecycleCallbacks(new MyLifecycleHandler());
    }
}
