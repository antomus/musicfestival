package com.fete.musicfestival.models;

import com.orm.SugarRecord;

/**
 * Created by antomus on 5/29/15.
 */
public class Dot extends SugarRecord<Dot> {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLtd() {
        return ltd;
    }

    public void setLtd(String ltd) {
        this.ltd = ltd;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    String name;
    String ltd;
    String lng;

    public Dot() {

    }

    public Dot(
            String name,
            String ltd,
            String lng
    ) {
        this.name = name;
        this.ltd = ltd;
        this.lng = lng;
    }

    public String toString() {
        return this.name;
    }

}
