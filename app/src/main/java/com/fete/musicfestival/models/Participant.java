package com.fete.musicfestival.models;

import android.util.Log;

import com.fete.musicfestival.utils.DataLoad;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static com.fete.musicfestival.utils.Constants.PRINT_DEBUG;

/**
 * Created by antomus on 5/29/15.
 */
public class Participant extends SugarRecord<Participant> {
    public Dot getDot() {
        return dot;
    }

    public void setDot(Dot dot) {
        this.dot = dot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameAscii() {
        return nameAscii;
    }

    public void setNameAscii(String nameAscii) {
        this.nameAscii = nameAscii;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    Dot dot;
    String name;
    String nameAscii;
    String desc;
    String img;
    String start;
    String finish;

    public Participant() {

    }

    public Participant(Dot dot,
                       String name,
                       String nameAscii,
                       String desc,
                       String img,
                       String start,
                       String finish) {
        this.dot = dot;
        this.name = name;
        this.nameAscii = nameAscii;
        this.desc = desc;
        this.img = img;
        this.start = start;
        this.finish = finish;
    }

    public String toString() {
        return this.name;
    }

    public static List<Participant> getParticipantsByDotId(Long dotId) {
        return Participant.find(Participant.class, "dot = ?", dotId + "");
    }

    public static List<Participant> getParticipantsFilteredByName(String name) {
        String query ;
        List<Participant> res = new ArrayList<>();
        name = ""+ DataLoad.toAscii(name)+"";

        if(name != null && !name.isEmpty()) {
                query = "name_ascii LIKE ? ";
                res = Participant.find(Participant.class, query, "%"+name+"%");
               if(PRINT_DEBUG) Log.e("Participant result: ", res.size()+"");
        }

        return res;
    }

    public static Boolean hastDotParticipantsInTime(Long dotId, String date) {
        String sql =  "dot = ? AND start <= Datetime(?) AND finish >= Datetime(?)";
        return Participant.find(Participant.class, sql, dotId + "", date, date).size() > 0;
    }




}
