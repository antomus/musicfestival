package com.fete.musicfestival;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.fete.musicfestival.models.Dot;
import com.fete.musicfestival.utils.Constants;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antomus on 6/12/15.
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected RoutesReceiver mReceiver = new RoutesReceiver();
    protected Toolbar myToolbar;
    protected ImageView homeButton;
    protected FrameLayout searchEdit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void setCustomActionBar() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        homeButton = (ImageView) myToolbar.findViewById(R.id.imageButton);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setAction(Intent.ACTION_SEND);
                startActivity(intent);
            }
        });

        Button close_search = (Button) myToolbar.findViewById(R.id.close_search);

        close_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearEditBox();
                hideEditBox();
            }
        });

        EditText search_line = (EditText) myToolbar.findViewById(R.id.search_line);

        search_line.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showData(s.toString().trim());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });
    }

    protected void showEditBox() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        homeButton = (ImageView) myToolbar.findViewById(R.id.imageButton);
        homeButton.setVisibility(View.GONE);
        searchEdit = (FrameLayout) myToolbar.findViewById(R.id.searchEdit);
        searchEdit.setVisibility(View.VISIBLE);
    }

    protected void hideEditBox() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        homeButton = (ImageView) myToolbar.findViewById(R.id.imageButton);
        homeButton.setVisibility(View.VISIBLE);
        searchEdit = (FrameLayout) myToolbar.findViewById(R.id.searchEdit);
        searchEdit.setVisibility(View.GONE);
    }

    protected void clearEditBox() {
        myToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        EditText search_line = (EditText) myToolbar.findViewById(R.id.search_line);
        search_line.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mReceiver);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        registerReceiver();
        super.onResume();
    }

    protected void registerReceiver() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
                this.mReceiver,
                new IntentFilter(
                        Constants.BROADCAST_ACTION));
    }


    protected abstract void showData(String search);

    ////Defining a BroadcastReceiver
    protected class RoutesReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Log.e("Base onReceive", context.getClass().getName());
            showData(null);
        }
    }
}
