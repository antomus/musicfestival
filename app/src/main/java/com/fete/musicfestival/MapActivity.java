package com.fete.musicfestival;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.FragmentTransaction;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;


import com.fete.musicfestival.models.Dot;
import com.fete.musicfestival.models.Participant;
import com.fete.musicfestival.utils.Constants;
import com.fete.musicfestival.utils.PopupAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {
    final static String LOG_TAG = "MapActivity";
    GoogleMap googleMap;
    LatLng myPosition;
    Marker mMarker;

    HashMap<Marker, Long> markersMap = new HashMap<Marker, Long>();

//    protected void registerReceiver() {
//        LocalBroadcastManager.getInstance(this).registerReceiver(
//                this.mReceiver,
//                new IntentFilter(
//                        Constants.BROADCAST_ACTION));
//    }
//
//
//    ////Defining a BroadcastReceiver
//    protected class RoutesReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            showData();
//        }
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        super.setCustomActionBar();

        if (!googleServiceOK()) return;

        if (!hasNetwork()) {
            showNetworkError();
            return;
        }

        registerReceiver();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
          .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onResume() {
        setDefaultRadio();
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        moveCameraToCityCoords();
        showData(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // îáíîâëåíèå ìåíþ
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, GPLicenseActivity.class);
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * function to load map. If map is not created it will create it for you
     */
    public boolean hasNetwork() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }

    private void moveCameraToCityCoords() {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.lvivCoords[0], Constants.lvivCoords[1]), getZoom()));
        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);
        // addMyLocation(googleMap.getMyLocation());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    private void setDefaultRadio() {
        RadioButton all = (RadioButton) findViewById(R.id.all);
        all.setChecked(true);
        RadioButton live = (RadioButton) findViewById(R.id.live);
        live.setChecked(false);
        RadioButton in_30 = (RadioButton) findViewById(R.id.in_30);
        in_30.setChecked(false);
    }

    @SuppressLint("MissingPermission")
    private void addMyLocation(Location location) {
        if (location == null) {
            LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            String provider = locManager.getBestProvider(criteria, false);

            location = locManager.getLastKnownLocation(provider);
            if (location != null) {
                //Log.i("addMyLocation getLastKnownLocation ", location.getLatitude() +" "+ location.getLongitude());
            }

        } else {
            //Log.i("addMyLocation", "location is not from  getLastKnownLocation" );
        }

        if (location != null) {
            //Log.i("addMyLocation myPosition ", location.getLatitude() +" "+ location.getLongitude());
            BigDecimal ltdPrec = new BigDecimal(Constants.lvivCoords[0] - location.getLatitude());
            BigDecimal lngPrec = new BigDecimal(Constants.lvivCoords[1] - location.getLongitude());
            if (!(ltdPrec.setScale(2, RoundingMode.HALF_UP).compareTo(BigDecimal.ZERO) == 0) &&
                    !(lngPrec.setScale(2, RoundingMode.HALF_UP).compareTo(BigDecimal.ZERO) == 0)) {
                return;
            }
            myPosition = new LatLng(location.getLatitude(), location.getLongitude());
            if (mMarker != null) mMarker.remove();
            if (googleMap != null) {
                mMarker = googleMap.addMarker(new MarkerOptions().position(myPosition).title("Me"));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myPosition, getZoom());
                googleMap.animateCamera(cameraUpdate);
            }
        }

    }

    @Override
    public void showData(String search) {

        List<Dot> dots = Dot.listAll(Dot.class);

            for (Dot dot : dots) {
//Log.e("showData Dot", "LNG:"+dot.getLng()+" "+"LTD:"+dot.getLtd());
                    Marker mark = googleMap.addMarker(new MarkerOptions().position(
                        new LatLng(Double.parseDouble(dot.getLng()), Double.parseDouble(dot.getLtd())))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));
                    mark.setTitle(Html.fromHtml(dot.getName()).toString());
                    markersMap.put(mark, dot.getId());
                    googleMap.setOnInfoWindowClickListener(
                        new OnInfoWindowClickListener() {
                            public void onInfoWindowClick(Marker marker) {
                                marker.hideInfoWindow();
                                Long dotId = markersMap.get(marker);
                                try {
                                    Intent intent = new Intent(MapActivity.this, ParticipantsActivity.class);
                                    intent.putExtra("dotId", dotId);
                                    startActivity(intent);
                                }catch (NullPointerException e) {
                                    Log.e("onInfoWindowClickError", dotId + "");
                                }

                                }
                            }
                    );
            }

        Long dotId = getIntent().getLongExtra("dotId", 0);
        if(dotId != null && dotId > 0) {
           zoomInDot(dotId);
        }
    }

    private void zoomInDot(Long dotId) {
        Dot dot  = Dot.findById(Dot.class, dotId);
        Log.e("zoomInDot null", (dot == null)+"");
        if(dot != null) {
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(dot.getLng()),
                            Double.parseDouble(dot.getLtd())));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(20);
            if (googleMap != null) {
                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);
            }
        }
    }

    public void appendLog(String text) {
//	   File logFile = new File("sdcard/log.file");
//	   if (!logFile.exists())
//	   {
//	      try
//	      {
//	         logFile.createNewFile();
//	      }
//	      catch (IOException e)
//	      {
//	         // TODO Auto-generated catch block
//	         e.printStackTrace();
//	      }
//	   }
//	   try
//	   {
//	      //BufferedWriter for performance, true to set append to file flag
//	      BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
//	      buf.append(text);
//	      buf.newLine();
//	      buf.close();
//	   }
//	   catch (IOException e)
//	   {
//	      // TODO Auto-generated catch block
//	      e.printStackTrace();
//	   }
    }

    public boolean googleServiceOK() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        Constants.MAPS_UNAVAILABLE).show();
            }

            return false;
        }

        return true;
    }

    private void showNetworkError() {

            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            // Setting Dialog Title
            dialog.setTitle(R.string.network_title);

            // Setting Dialog Message
            dialog.setMessage(R.string.network_error);
            dialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            dialog.dismiss();
                        }
                    });
            dialog.show();
    }


    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId())  {
            case   R.id.all:
                if(checked) {
                   for (Marker key : markersMap.keySet()) {
                      key.setVisible(true);
                   }
                }
            break;
            case R.id.live:
                if(checked) {
                     for (Marker key : markersMap.keySet()) {
                         Long dotId = markersMap.get(key);
                         String now = formatDate(new Date());
                         key.setVisible(Participant.hastDotParticipantsInTime(dotId, now));
                     }
                }
            break;
            case R.id.in_30:
                if(checked) {
                    for (Marker key : markersMap.keySet()) {
                        Long dotId = markersMap.get(key);
                        final Date now = new Date();
                        final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs
                        long t = now.getTime();
                        Date afterAddingMins = new Date(t + (30 * ONE_MINUTE_IN_MILLIS));
                        String strDate = formatDate(afterAddingMins);
                        key.setVisible(Participant.hastDotParticipantsInTime(dotId, strDate));
                    }
                }
                break;
        }
    }


    @SuppressLint("SimpleDateFormat")
        String formatDate(Date date) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Log.e("DATE", "DATE " + date.toString());
        return sf.format(date);
    }

    int getZoom() {
        int zoom = 12;
        Point size = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
//	    Log.e("width ", "SCREEN WIDTH " +size.x);
//	    Log.e("height ", "SCREEN HEIGHT " +size.y);
        if (size.x > 1100 || size.y > 1100) {
            zoom = 14;
        }
        return zoom;
    }


}