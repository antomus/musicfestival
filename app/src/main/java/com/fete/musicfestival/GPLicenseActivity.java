package com.fete.musicfestival;

import com.google.android.gms.common.GooglePlayServicesUtil;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class GPLicenseActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gplicense);
        super.setCustomActionBar();
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        String license = GooglePlayServicesUtil.getOpenSourceSoftwareLicenseInfo(this);
        TextView tv = (TextView) findViewById(R.id.gp_license_text);
        if (license != null) {
            tv.setText(license);
        } else {
            tv.setText(R.string.no_gp);
        }
    }

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.gplicense, menu);
//		return true;
//	}
    @Override
    protected void showData(String search){

    }

}
