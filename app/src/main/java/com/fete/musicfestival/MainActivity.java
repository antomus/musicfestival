package com.fete.musicfestival;



import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fete.musicfestival.utils.Constants;
import com.fete.musicfestival.utils.NetworkTask;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        super.setCustomActionBar();

        TextView about = (TextView) findViewById(R.id.about);
        TextView map = (TextView) findViewById(R.id.map);
        TextView locations = (TextView) findViewById(R.id.locations);
        TextView participants = (TextView) findViewById(R.id.participants);
        ImageView facebook = (ImageView) findViewById(R.id.facebook);
        ImageView instagram = (ImageView) findViewById(R.id.instagram);
        ImageView youtube = (ImageView) findViewById(R.id.youtube);

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), AboutActivity.class);
                intent.setAction(Intent.ACTION_SEND);
                startActivity(intent);
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), MapActivity.class);
                intent.setAction(Intent.ACTION_SEND);
                startActivity(intent);
            }
        });
        locations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), LocationsActivity.class);
                intent.setAction(Intent.ACTION_SEND);
                startActivity(intent);
            }
        });
        participants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), ParticipantsActivity.class);
                intent.setAction(Intent.ACTION_SEND);
                startActivity(intent);
            }
        });
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(Constants.FACEBOOK_URL));
                startActivity(intent);
            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(Constants.INSTAGRAM_URL));
                startActivity(intent);
            }
        });
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(Constants.YOUTUBE_URL));
                startActivity(intent);
            }
        });
    }


    public void onResume() {
        super.onResume();
        NetworkTask net = new NetworkTask(this);
        net.execute();
    }

    @Override
    protected void showData(String search){

    }
}