package com.fete.musicfestival.utils;

public class Constants {
    // Action Name to IntentFilter
    public static final String BROADCAST_ACTION = "com.example.musicfestival.BROADCAST";

    public static final String DOTS_LIST = "com.example.musicfestival.DOTS_LIST";

    public static final String ROUTES_LIST = "com.example.musicfestival.ROUTES_LIST";

    public static final String DOTS_PREF = "DOTS_PREF";

    public static final String PREF_UPDATE_TIME = "PREF_UPDATE_TIME";

    // The url to the server 
    public static final String SERVER_URL = "https://fetedelamusique.lviv.ua/api/events/get_all";

    public static final String ABOUT_URL = "https://fetedelamusique.lviv.ua/?json=get_page&slug=about";
    public static final String MAP_TILE_URL = "https://maps.googleapis.com/maps/api/staticmap?center=[]&zoom=15&size=125x125&sensor=false&key=AIzaSyAAbs-dHNRoEUvWQ5UIqtd8XmRJjdNsaos";

    public static final String PREFS_NAME = "MusicFestPrefs";


    public static final double[] lvivCoords = {49.84, 24.03};

    public static final int updateTime = 630000;

    public static final int MAPS_UNAVAILABLE = 9001;

    public static final String UNKNOWN_PLACE = "\u041c\u0456\u0441\u0446\u0435 \u0432\u0438\u0441\u0442\u0443\u043f\u0443 \u0443\u0442\u043e\u0447\u043d\u044e\u0454\u0442\u044c\u0441\u044f";

    public static final String ALL = "ALL";
    public static final String LIVE = "LIVE";
    public static final String IN_30 = "IN_30";
    public  static final String FACEBOOK_URL = "https://www.facebook.com/fetedelamusique.lviv";
    public  static final String INSTAGRAM_URL = "https://instagram.com/fetedelamusiquelviv";
    public  static final String YOUTUBE_URL = "https://www.youtube.com/user/fetedelamusiquelviv/videos";
    public static final boolean PRINT_DEBUG = false;
}
