package com.fete.musicfestival.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.fete.musicfestival.models.Participant;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.io.File;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

/**
 * Created by antomus on 3/30/15.
 */
public class NetworkTask extends AsyncTask<Void, Void, Void> {

    WeakReference<Context> weakContext;

    public NetworkTask(Context context) {

        weakContext = new WeakReference<>(context);
    }

    protected Void doInBackground(Void... params) {
        boolean isOldDb = false;
        boolean participantsListIsEmpty = Participant.count(Participant.class, null, null) == 0;
        File db;

        if (weakContext.get() != null) {
            db = new File(weakContext.get().getApplicationInfo().dataDir + "/databases/fete.db");
            isOldDb = dbIsOutdated(db);
        }

        if ( participantsListIsEmpty || isOldDb ) {
            String data = DataLoad.getHTTPData(Constants.SERVER_URL);
            DataLoad.saveEventsAndLocations(data);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void data) {
        super.onPostExecute(data);
//        if(weakContext.get() != null && MyLifecycleHandler.isApplicationVisible()) {
//            Intent i = new Intent(weakContext.get().getApplicationContext(), MainActivity.class);
//            weakContext.get().startActivity(i);
//            ((Activity) weakContext.get()).finish();
//        }
        //Log.d("onHandleIntent Exception", "onHandleIntent result2 " + result.toString());
        // Creating an intent for broadcastreceiver
        Intent broadcastIntent = new Intent(Constants.BROADCAST_ACTION);
        //broadcastIntent.set
        // Attaching data to the intent
        //Log.d("onHandleIntent", broadcastIntent.getData().toString());
        // Sending the broadcast
        // Log.i("onHandleIntent", "result " + result.toString());
        LocalBroadcastManager.getInstance(weakContext.get().getApplicationContext()).sendBroadcast(broadcastIntent);
    }

    private boolean dbIsOutdated(File db) {
        long lastMod = db.lastModified();
        Calendar today = Calendar.getInstance();
        today.clear(Calendar.HOUR);
        today.clear(Calendar.MINUTE);
        today.clear(Calendar.SECOND);
        Date todayDate = today.getTime();
        int days = Days.daysBetween(new DateTime(todayDate), new DateTime(lastMod)).getDays();
        days = Math.abs(days);
        return days > 1;
    }


}
