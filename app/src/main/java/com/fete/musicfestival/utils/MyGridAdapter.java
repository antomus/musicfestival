package com.fete.musicfestival.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.fete.musicfestival.LocationsActivity;
import com.fete.musicfestival.ParticipantDetailsActivity;
import com.fete.musicfestival.R;
import com.fete.musicfestival.models.Participant;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antomus on 6/13/15.
 */
public class MyGridAdapter extends BaseAdapter  implements Filterable{
    private List<Participant> mItems = new ArrayList<>();
    private final LayoutInflater mInflater;
    private final Activity context;

    public MyGridAdapter(Activity context, List<Participant> participantsList) {
        mItems = participantsList;
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Participant getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        TextView text;
        if (convertView == null) {
            convertView = context.getLayoutInflater()
                    .inflate(R.layout.grid_item, parent, false);
            imageView = (ImageView) convertView
                    .findViewById(R.id.picture);
            text = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(new Item(text, imageView));
        } else {
            Item viewHolder = (Item) convertView.getTag();
            imageView = viewHolder.img;
            text = viewHolder.name;
        }

        final Participant item = getItem(position);

        text.setText(Html.fromHtml(item.getName()));

        imageView.setImageResource(R.drawable.nav_box);
//        Picasso.with(context)
//                .load(item.getImg())
//               // .placeholder(R.drawable.nav_box)
//                .into(imageView);

        Ion.with(imageView)
                .placeholder(R.drawable.grid_placeholder)
                .error(R.drawable.fete_de_la_musique_small)

                .load(item.getImg());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ParticipantDetailsActivity.class);
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra("id", item.getId());
                context.startActivity(intent);

            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    private static class Item {
        public final TextView name;
        public final ImageView img;

        Item(TextView name, ImageView img) {
            this.name = name;
            this.img = img;
        }
    }
}
