package com.fete.musicfestival.utils;

import android.util.Log;

import com.fete.musicfestival.models.Dot;
import com.fete.musicfestival.models.Participant;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import gcardone.junidecode.Junidecode;

import static com.fete.musicfestival.utils.Constants.PRINT_DEBUG;


/**
 * DataLoad is an utility class responsible for fetching data from remote server and saving it to db.
 */

public class DataLoad {

    public static String getHTTPData(String strUrl) {
        if(PRINT_DEBUG)Log.e("getHTTPData", " getHTTPData");
        String data = "";
        String line;
        InputStream iStream;
        BufferedReader br;
        HttpResponse response;
        HttpClient httpclient = new DefaultHttpClient();

        try {
            HttpGet method = new HttpGet(strUrl);
            response = httpclient.execute(method);
            iStream = response.getEntity().getContent();
            //Log.d(LOG_TAG, "The response is: " + response.getStatusLine());

            br = new BufferedReader(new InputStreamReader(iStream));
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            if(PRINT_DEBUG)Log.d("getHTTPData", data + "");
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.e("getHTTPData err1", e.toString());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("getHTTPData err2", e.toString());
        }

        return data;
    }

    private static void saveEvents(Dot loc, JSONArray evs) throws JSONException {
        if(PRINT_DEBUG)Log.e("saveEvents", "saveEvents " + evs.length() + " ");
            for (int j = 0, arrLen = evs.length(); j < arrLen; j++) {
                JSONObject event = evs.getJSONObject(j);
                JSONObject dateObj = event.getJSONObject("date");
                String dateStartStr = dateObj.getString("StartDate") + " " + dateObj.getString("StartTime");
                String dateEndStr = dateObj.getString("EndDate") + " " + dateObj.getString("FinishTime");

                Participant evt = new Participant(loc,
                  event.getString("name"),
                  DataLoad.toAscii(event.getString("name")),
                  event.getString("desc"),
                  event.getString("img"),
                  dateStartStr,
                  dateEndStr);
                if(PRINT_DEBUG)Log.e("Participant", "Participant " + evt.toString() + " ");
                evt.save();
            }


    }

    public static void saveEventsAndLocations(String data) {
        JSONObject jObject;

        try {
            jObject = new JSONObject(data);
            jObject.remove("status");
            if(PRINT_DEBUG)Log.e("jObject", "jObject " + jObject.length() + " ");
            JSONObject loopObj;

            Iterator<String> keys = jObject.keys();
            if(PRINT_DEBUG)Log.e("jObject keys", jObject.keys().toString());
            while (keys.hasNext()) {
                String key = keys.next();
                if(PRINT_DEBUG)Log.e("saveEvents key", key);
                if (!key.equals("status")) {
                    loopObj = jObject.getJSONObject(key);
                    //Log.e("getDots result","getDots loopObj "+loopObj.toString());
                    if (!loopObj.isNull("lat") && !loopObj.isNull("lng")) {
                        Dot loc = new Dot(loopObj.optString("name"),
                                loopObj.optString("lng"),
                                loopObj.optString("lat"));
                        loc.save();
                        if(PRINT_DEBUG)Log.e("latlng_ok", "saveEvents Ok " );
                        saveEvents(loc, loopObj.getJSONArray("events"));
                    }
                }
            }
            if(PRINT_DEBUG) Log.e("saveEvents Ok", "saveEvents Ok " );

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("saveEvents Ex", "saveEvents()Ex " + e.toString());
        }
    }

    public static String toAscii(String str){
        return Junidecode.unidecode(str);
    }


}